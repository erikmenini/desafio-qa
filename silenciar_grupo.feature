# language: pt
# encoding: utf-8

Funcionalidade: Silenciar grupo

Cenário: Silenciar grupo por 8 horas
      Dado que eu esteja na aba de "Conversas"
      Quando pressiono na conversa de um grupo
      E clico as configurações do grupo
      E clique eem "Silenciar"
      E seleciono a opção "8 Horas"
      E Clique no botão "Ok"
      Então o grupo é silenciado por "8 horas"


Cenário: Silenciar grupo por 1 semana
      Dado que eu esteja na aba de "Conversas"
      Quando pressiono na conversa de um grupo
      E clico as configurações do grupo
      E clique eem "Silenciar"
      E seleciono a opção "1 semana"
      E Clique no botão "Ok"
      Então o grupo é silenciado por "1 semana"

Cenário: Silenciar grupo por 1 ano
      Dado que eu esteja na aba de "Conversas"
      Quando pressiono na conversa de um grupo
      E clico as configurações do grupo
      E clique eem "Silenciar"
      E seleciono a opção "1 ano"
      E Clique no botão "Ok"
      Então o grupo é silenciado por "1 ano"

Cenário: Retirar o silenciar do grupo
      Dado que eu esteja na aba de "Conversas"
      Quando pressiono na conversa de um grupo
      E clico nas configurações do grupo
      E clico em "Não selecionar"
      Então o grupo não é mais silenciado
