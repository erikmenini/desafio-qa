# language: pt
# encoding: utf-8

Funcionalidade: Configurar privacidade da conta do usuário

Cenário: Configurar privacidade da foto do perfil para ser visualizado somente pelos contatos
      Dado que eu estou na tela "Privacidade"
      Quando pressiono em "Foto do perfil˜
      E seleciono "Meus contatos"
      Então privacidade de "Foto do perfil" alterado para "Meus Contatos"

Cenário: Configurar privacidade de visto por último para ninguém visualizar
      Dado que eu estou na tela "Privacidade"
      Quando pressiono em "Visto por último"
      E seleciono "Nenhum"
      Então privacidade de "Visto por último" altearado para "Nenhum"

Cenário: Configurar privacidade do Status do perfil
      Dado que eu estou na tela "Privacidade"
      Quando pressiono em "Status"
      E seleciono "Todos"
      Então privacidade do "Status" é alterada para "Todos"

Cenário: Configurar privacidade de confirmação de leitura
      Dado que eu estou na tela "Privacidade"
      Quando desmarco a checkbox de "Confirmações de leitura"
      Então "Confirmações de leitura" é desativada.
